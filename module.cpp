#include <napi.h>
#include "./module.h"
#include "./bubbleSort.cpp"

napi_value Sort(const Napi::CallbackInfo &info){
  Napi::Env env = info.Env();

  const Napi::Array inputArray = info[0].As<Napi::Array>();

  unsigned int length = inputArray.Length();
  unsigned int array[length];
  unsigned int i;

  for (i = 0; i < length; i++) {
    array[i] = inputArray[i].As<Napi::Number>().Uint32Value();
  }

  unsigned int *arrayPointer = &array[0];

  bubbleSort(arrayPointer, length);

  Napi::Array outputArray = Napi::Array::New(env, length);

  for (i = 0; i < length; i++) {
    outputArray[i] = Napi::Number::New(env, uint32_t(array[i]));
  }

  return outputArray;

}

Napi::Object Init(Napi::Env env, Napi::Object exports){
  exports.Set(Napi::String::New(env, "sort"), Napi::Function::New(env, Sort));
  return exports;
}

NODE_API_MODULE(module, Init)
