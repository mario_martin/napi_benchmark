import java.util.Random;

class BubbleSort { 
    void bubbleSort(int arr[]) { 
        int n = arr.length; 
        for (int i = 0; i < n-1; i++) 
            for (int j = 0; j < n-i-1; j++) 
                if (arr[j] > arr[j+1]) 
                { 
                    int temp = arr[j]; 
                    arr[j] = arr[j+1]; 
                    arr[j+1] = temp; 
                } 
    } 
    public static void main(String args[]) { 
        BubbleSort ob = new BubbleSort(); 
        Random r = new Random();
        int[] arr = new int[50000]; 
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) r.nextInt(50000);
        }

		long startTime = System.currentTimeMillis();
        ob.bubbleSort(arr); 
		long endTime = System.currentTimeMillis();

        System.out.println("Java bubble sort: "+ (endTime - startTime));
        System.out.println();
        System.out.println();
    }
} 