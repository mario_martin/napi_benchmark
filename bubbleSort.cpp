#include <stdio.h>

void swap(unsigned int *a, unsigned int *b) {
  unsigned int temp;
  temp = *a;
  *a = *b;
  *b = temp;
}

void bubbleSort(unsigned int *array, unsigned int length) {
  unsigned int i, j;
  for (i = 0; i < (length - 1); ++i) {
    for (j = 0; j < length - 1 - i; ++j) {
      if (*(array + j) > *(array + j + 1)) {
        swap(array + j, array + j + 1);
      }
    }
  }
}


