const BUBBLE_SORT = 1;
const ARRAY_LENGTH = 50000;

const targetArray = Array(ARRAY_LENGTH).fill(null).map(() => Math.round(Math.random() * 1000));
const sortedArrayJSBubble = [...targetArray];

console.log("");
console.log("");
console.time('NodeJS (Javascript) bubble sort');

for (let i = 0; i < sortedArrayJSBubble.length - 1; ++i) {
  for (let j = 0; j < sortedArrayJSBubble.length - 1 - i; ++j) {
    if (sortedArrayJSBubble[j] > sortedArrayJSBubble[j + 1]) {
      const temp = sortedArrayJSBubble[j + 1];
      sortedArrayJSBubble[j + 1] = sortedArrayJSBubble[j];
      sortedArrayJSBubble[j] = temp;
    }
  }
}

console.timeEnd('NodeJS (Javascript) bubble sort');
console.log("");
console.log("");