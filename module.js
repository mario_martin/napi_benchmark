const addon = require('./build/Release/module.node');

const BUBBLE_SORT = 1;
const ARRAY_LENGTH = 50000;

const targetArray = Array(ARRAY_LENGTH).fill(null).map(() => Math.round(Math.random() * 1000));

console.log("");
console.log("");

console.time('NodeJS (C++) bubble sort');
addon.sort(targetArray, BUBBLE_SORT);
console.timeEnd('NodeJS (C++) bubble sort');

console.log("");
console.log("");